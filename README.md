# IUTB-ANTI UA

Cette application android est un outil pour créer des alarmes automatiquement pour ne pas arriver en retard.

Elle est mise en accés totalement libre (licence WTFPL ) : https://fr.wikipedia.org/wiki/WTFPL

Créateur : Théophile Orliac

Etapes de développement :
* V1 : Connexion à l'edt et ajout de l'alarme en fonction de l'année/groupe
* V2 (actuel) : Possibilité de l'utiliser en temps que enseignant
* V3 (en developemment) : Si activée, fonctionalité d'ajout automatique des alarmes à une heure spécifiée, tous les jours
* V4 : Ajout de l'emploi du temps à un compte Google (à la place de passer par un PC)
* V5 : Proposez !
* 
>>>
vous pouvez voir le code source mais pour pouvoir push contactez theo.orliac@gmail.com
>>>

## Exemple d'utilisation
Saisir l'année de promo puis le groupe de TD/TP

Saisir le temps en minute avant le premier cour de la journée

Appuyer sur le bouton 

<img src="https://gitlab.iut-blagnac.fr/torliac/iutb-anti-ua/raw/master/Images/Screenshot_empty.png" alt="drawing" width="300"/>
<img src="https://gitlab.iut-blagnac.fr/torliac/iutb-anti-ua/raw/master/Images/Screenshot_filled.png" alt="drawing" width="300"/>
<img src="https://gitlab.iut-blagnac.fr/torliac/iutb-anti-ua/raw/master/Images/Screenshot_20181119-143912.png" alt="drawing" width="300"/>